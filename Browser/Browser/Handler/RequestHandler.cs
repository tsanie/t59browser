﻿using CefSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browser.Browser.Handler
{
    public class RequestHandler : IRequestHandler
    {
        bool IRequestHandler.GetAuthCredentials( IWebBrowser browserControl, IBrowser browser, IFrame frame, bool isProxy, string host, int port, string realm, string scheme, IAuthCallback callback )
        {
            //NOTE: If you do not wish to implement this method returning false is the default behaviour
            // We also suggest you explicitly Dispose of the callback as it wraps an unmanaged resource.

            callback.Dispose();
            return false;
        }

        IResponseFilter IRequestHandler.GetResourceResponseFilter( IWebBrowser browserControl, IBrowser browser, IFrame frame, IRequest request, IResponse response )
        {

            //if ( request.Url.EndsWith( "kcs_flash.js" ) )
            //{
            //    // todo:
            //    return new FindReplaceResponseFilter( "\"opaque\"", "\"direct\"" );
            //}

            return null;
        }

        bool IRequestHandler.OnBeforeBrowse( IWebBrowser browserControl, IBrowser browser, IFrame frame, IRequest request, bool isRedirect )
        {
            return false;
        }

        CefReturnValue IRequestHandler.OnBeforeResourceLoad( IWebBrowser browserControl, IBrowser browser, IFrame frame, IRequest request, IRequestCallback callback )
        {
            //Console.WriteLine( "[{0:MM/dd/yyyy HH:mm:ss.fff}]", DateTime.Now );
            //Console.WriteLine( "Url: {0}", request.Url );
            //Console.WriteLine( "Headers:" );
            //foreach ( var key in request.Headers.AllKeys )
            //{
            //    Console.WriteLine( "    {0}: {1}", key, request.Headers[key] );
            //}

            //Example of how to set Referer
            // Same should work when setting any header

            // For this example only set Referer when using our custom scheme
            //var url = new Uri( request.Url );
            //if ( url.Scheme == "http" )
            //{
            //    var headers = request.Headers;

            //    headers["Custom-Field"] = "reuse";

            //    request.Headers = headers;
            //}

            return CefReturnValue.Continue;
        }

        bool IRequestHandler.OnCertificateError( IWebBrowser browserControl, IBrowser browser, CefErrorCode errorCode, string requestUrl, ISslInfo sslInfo, IRequestCallback callback )
        {
            //NOTE: If you do not wish to implement this method returning false is the default behaviour
            // We also suggest you explicitly Dispose of the callback as it wraps an unmanaged resource.
            //callback.Dispose();
            //return false;

            //NOTE: When executing the callback in an async fashion need to check to see if it's disposed
            if ( !callback.IsDisposed )
            {
                using ( callback )
                {
                    //To allow certificate
                    callback.Continue( true );
                    return true;
                }
            }

            return false;
        }

        bool IRequestHandler.OnOpenUrlFromTab( IWebBrowser browserControl, IBrowser browser, IFrame frame, string targetUrl, WindowOpenDisposition targetDisposition, bool userGesture )
        {
            return false;
        }

        void IRequestHandler.OnPluginCrashed( IWebBrowser browserControl, IBrowser browser, string pluginPath )
        {
            // TODO: Add your own code here for handling scenarios where a plugin crashed, for one reason or another.
        }

        bool IRequestHandler.OnProtocolExecution( IWebBrowser browserControl, IBrowser browser, string url )
        {
            return url.StartsWith( "mailto" );
        }

        bool IRequestHandler.OnQuotaRequest( IWebBrowser browserControl, IBrowser browser, string originUrl, long newSize, IRequestCallback callback )
        {
            //NOTE: If you do not wish to implement this method returning false is the default behaviour
            // We also suggest you explicitly Dispose of the callback as it wraps an unmanaged resource.
            //callback.Dispose();
            //return false;

            //NOTE: When executing the callback in an async fashion need to check to see if it's disposed
            if ( !callback.IsDisposed )
            {
                using ( callback )
                {
                    //Accept Request to raise Quota
                    //callback.Continue(true);
                    //return true;
                }
            }

            return false;
        }

        void IRequestHandler.OnRenderProcessTerminated( IWebBrowser browserControl, IBrowser browser, CefTerminationStatus status )
        {
            // TODO: Add your own code here for handling scenarios where the Render Process terminated for one reason or another.
            //browserControl.Load( CefExample.RenderProcessCrashedUrl );
        }

        void IRequestHandler.OnRenderViewReady( IWebBrowser browserControl, IBrowser browser )
        {
        }

        void IRequestHandler.OnResourceLoadComplete( IWebBrowser browserControl, IBrowser browser, IFrame frame, IRequest request, IResponse response, UrlRequestStatus status, long receivedContentLength )
        {
        }

        void IRequestHandler.OnResourceRedirect( IWebBrowser browserControl, IBrowser browser, IFrame frame, IRequest request, ref string newUrl )
        {
            //Example of how to redirect - need to check `newUrl` in the second pass
            //if (string.Equals(frame.GetUrl(), "https://www.google.com/", StringComparison.OrdinalIgnoreCase) && !newUrl.Contains("github"))
            //{
            //	newUrl = "https://github.com";
            //}
        }

        bool IRequestHandler.OnResourceResponse( IWebBrowser browserControl, IBrowser browser, IFrame frame, IRequest request, IResponse response )
        {
            //NOTE: You cannot modify the response, only the request
            // You can now access the headers
            //var headers = response.ResponseHeaders;

            return false;
        }
    }
}
