﻿using CefSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Browser.Browser.Handler
{
    public class KeyboardHandler : IKeyboardHandler
    {
        private FormBrowser browser;

        public KeyboardHandler( FormBrowser browser )
        {
            this.browser = browser;
        }

        bool IKeyboardHandler.OnKeyEvent( IWebBrowser browserControl, IBrowser browser, KeyType type, int windowsKeyCode, int nativeKeyCode, CefEventFlags modifiers, bool isSystemKey )
        {
            return false;
        }

        bool IKeyboardHandler.OnPreKeyEvent( IWebBrowser browserControl, IBrowser browser, KeyType type, int windowsKeyCode, int nativeKeyCode, CefEventFlags modifiers, bool isSystemKey, ref bool isKeyboardShortcut )
        {
            if ( windowsKeyCode == (int)System.Windows.Forms.Keys.F2 && type == KeyType.KeyUp && modifiers == CefEventFlags.None )
            {
                this.browser?.TakeScreenShot();
            }
            return false;
        }
    }
}
