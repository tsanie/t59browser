﻿//#define DEBUG

using CefSharp;
using CefSharp.BrowserSubprocess;
using CefSharp.Internals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Browser
{
    static class Program
    {
        const string PEPFLASH_NAME = "pepflashplayer.dll";

        private static CefSubProcess Create( IEnumerable<string> args )
        {
            const string typePrefix = "--type=";
            var typeArgument = args.SingleOrDefault( arg => arg.StartsWith( typePrefix ) );
            if ( string.IsNullOrEmpty( typeArgument ) )
                return null;

            var wcfEnabled = args.Any( arg => arg.StartsWith( CefSharpArguments.WcfEnabledArgument ) );

            var type = typeArgument.Substring( typePrefix.Length );
            switch ( type )
            {
                case "renderer":
                {
                    return wcfEnabled ? new CefRenderProcess( args ) : new CefSubProcess( args );
                }
                case "gpu-process":
                default:
                {
                    return new CefSubProcess( args );
                }
            }
        }

        [STAThread]
        static int Main( string[] args )
        {
            // sub process
            var subprocess = Create( args );
            if (subprocess != null )
            {
                CefAppWrapper.EnableHighDPISupport();
                int result;

                using ( subprocess )
                {
                    result = subprocess.Run();
                }

                return result;
            }

            //Application.SetUnhandledExceptionMode( UnhandledExceptionMode.CatchException );
            Application.ThreadException += Application_ThreadException;

            if ( args.Length == 0 )
            {
                MessageBox.Show( "此浏览器无法单独启动，请直接运行本体程序。", "消息", MessageBoxButtons.OK, MessageBoxIcon.Information );
                return -1;
            }

            var exitCode = Cef.ExecuteProcess();

            if ( exitCode >= 0 )
            {
                MessageBox.Show( "Cef 启动进程出错：" + exitCode, "错误", MessageBoxButtons.OK, MessageBoxIcon.Error );
                return exitCode;
            }

            var codebase = new Uri( System.Reflection.Assembly.GetEntryAssembly().CodeBase ).LocalPath;
            var path = System.IO.Path.Combine( System.IO.Path.GetDirectoryName( codebase ), "userdata" );

            var settings = new CefSettings();
            settings.BrowserSubprocessPath = System.IO.Path.GetFileName( codebase );
            settings.LogSeverity = LogSeverity.Default;
            settings.UserDataPath = path;
            settings.CachePath = System.IO.Path.Combine( path, "Cache" );
            if ( System.IO.File.Exists( PEPFLASH_NAME ) )
            {
                try
                {
                    var ver = System.Diagnostics.FileVersionInfo.GetVersionInfo( PEPFLASH_NAME );
                    settings.CefCommandLineArgs.Add( "ppapi-flash-path", PEPFLASH_NAME );
                    settings.CefCommandLineArgs.Add( "ppapi-flash-version", ver.FileVersion.Replace( ',', '.' ) );
                }
                catch ( Exception ex )
                {
                    MessageBox.Show( "pepperflash 加载失败：" + ex.ToString(), "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning );
                }
            }
            settings.MultiThreadedMessageLoop = true;
            Cef.OnContextInitialized = delegate
            {
                var cookieManager = Cef.GetGlobalCookieManager();
                cookieManager.SetStoragePath( path, true );
                cookieManager.SetSupportedSchemes( "custom" );
            };
            Cef.Initialize( settings, true, false );

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault( false );
            try
            {
                Application.Run( new FormBrowser( args[0] ) );
            }
            catch ( Exception ex )
            {
                Application_ThreadException( null, new System.Threading.ThreadExceptionEventArgs( ex ) );
            }

            return 0;
        }

        static void Application_ThreadException( object sender, System.Threading.ThreadExceptionEventArgs e )
        {
            MessageBox.Show( e.Exception.ToString(), "EOBrowser", MessageBoxButtons.OK, MessageBoxIcon.Error );
        }
    }
}
