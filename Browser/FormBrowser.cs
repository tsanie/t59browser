﻿using Browser.Browser.Handler;
using BrowserLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.ServiceModel;
using System.Windows.Forms;

namespace Browser
{
    /// <summary>
    /// ブラウザを表示するフォームです。
    /// </summary>
    /// <remarks>thx ElectronicObserver!</remarks>
    [ServiceBehavior( InstanceContextMode = InstanceContextMode.Single )]
    public partial class FormBrowser : Form, IBrowser
    {
        //private const string OFFSET_BASIS = "2166136261";
        private const string GAME_URL = "http://www.dmm.com/netgame/social/-/gadgets/=/app_id=854854";

        private readonly Size KanColleSize = new Size( 800, 480 );

        private const string FRAME_FILE = "FrameApply.js";
        private const string FRAME_RESTORE_FILE = "FrameRestore.js";
        private string FrameScript;
        private string FrameRestoreScript;


        // FormBrowserHostの通信サーバ
        private string ServerUri;

        // FormBrowserの通信サーバ
        private PipeCommunicator<IBrowserHost> BrowserHost;

        private BrowserLib.BrowserConfiguration Configuration;

        // 親プロセスが生きているか定期的に確認するためのタイマー
        private Timer HeartbeatTimer = new Timer();
        private IntPtr HostWindow;

        private bool _styleSheetApplied;
        /// <summary>
        /// スタイルシートの変更が適用されているか
        /// </summary>
        private bool StyleSheetApplied
        {
            get { return _styleSheetApplied; }
            set
            {

                if ( value )
                {
                    //Browser.Anchor = AnchorStyles.None;
                    SizeAdjuster_SizeChanged( null, new EventArgs() );

                }
                else
                {
                    SizeAdjuster.SuspendLayout();
                    //Browser.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                    Browser.Location = new Point( 0, 0 );
                    Browser.MinimumSize = new Size( 0, 0 );
                    Browser.Size = SizeAdjuster.Size;
                    SizeAdjuster.ResumeLayout();
                }

                _styleSheetApplied = value;
            }
        }

        /// <summary>
        /// 艦これが読み込まれているかどうか
        /// </summary>
        private bool IsKanColleLoaded { get; set; }

#if !NOVOL
        private VolumeManager _volumeManager;
#endif


        private NumericUpDown ToolMenu_Other_Volume_VolumeControl
        {
            get { return (NumericUpDown)( (ToolStripControlHost)ToolMenu_Other_Volume.DropDownItems["ToolMenu_Other_Volume_VolumeControlHost"] ).Control; }
        }


        /// <summary>
        /// </summary>
        /// <param name="serverUri">ホストプロセスとの通信用URL</param>
        public FormBrowser( string serverUri )
        {
            SuspendLayout();
            InitializeComponent();

            ServerUri = serverUri;
            StyleSheetApplied = false;
#if !NOVOL
            _volumeManager = new VolumeManager( (uint)System.Diagnostics.Process.GetCurrentProcess().Id );
#endif

            // ChromiumWebBrowser
            {
                Browser.RequestHandler = new RequestHandler();
                Browser.KeyboardHandler = new KeyboardHandler( this );
            }

            // 音量設定用コントロールの追加
            {
                var control = new NumericUpDown();
                control.Name = "ToolMenu_Other_Volume_VolumeControl";
                control.Maximum = 100;
                control.TextAlign = HorizontalAlignment.Right;
                control.Font = ToolMenu_Other_Volume.Font;

                control.ValueChanged += ToolMenu_Other_Volume_ValueChanged;
                control.Tag = false;

                var host = new ToolStripControlHost( control, "ToolMenu_Other_Volume_VolumeControlHost" );

                control.Size = new Size( host.Width - control.Margin.Horizontal, host.Height - control.Margin.Vertical );
                control.Location = new Point( control.Margin.Left, control.Margin.Top );


                ToolMenu_Other_Volume.DropDownItems.Add( host );
            }

            if ( System.IO.File.Exists( FRAME_FILE ) )
            {
                FrameScript = System.IO.File.ReadAllText( FRAME_FILE );
            }
            else
            {
                FrameScript = Properties.Resources.FrameScript;
            }
            if ( System.IO.File.Exists( FRAME_RESTORE_FILE ) )
            {
                FrameRestoreScript = System.IO.File.ReadAllText( FRAME_RESTORE_FILE );
            }
            else
            {
                FrameRestoreScript = Properties.Resources.FrameRestoreScript;
            }

            this.AutoScaleMode = AutoScaleMode.Dpi;
            this.AutoScaleDimensions = new SizeF( 96F, 96F );
            ResumeLayout();

        }

        private void Browser_AddressChanged( object sender, CefSharp.AddressChangedEventArgs e )
        {
            ToolMenu_Url.Text = e.Address;
        }

        private void Browser_LoadingStateChanged( object sender, CefSharp.LoadingStateChangedEventArgs e )
        {
            this.BeginInvoke( new Action<object>( o =>
            {
                if ( (bool)o )
                {
                    // is loading
                    ToolMenu_Loading.Visible = true;
                }
                else
                {
                    ToolMenu_Loading.Visible = false;
                    StyleSheetApplied = false;

                    ApplyStyleSheet();
                }
            } ), e.IsLoading );
        }

        private void FormBrowser_Load( object sender, EventArgs e )
        {
            SetWindowLong( this.Handle, GWL_STYLE, WS_CHILD );

            // ホストプロセスに接続
            BrowserHost = new PipeCommunicator<IBrowserHost>(
                this, typeof( IBrowser ), ServerUri + "Browser", "Browser" );
            BrowserHost.Connect( ServerUri + "/BrowserHost" );
            BrowserHost.Faulted += BrowserHostChannel_Faulted;


            ConfigurationChanged( BrowserHost.Proxy.Configuration );


            // ウィンドウの親子設定＆ホストプロセスから接続してもらう
            BrowserHost.Proxy.ConnectToBrowser( this.Handle );

            // 親ウィンドウが生きているか確認 
            HeartbeatTimer.Tick += (EventHandler)( ( sender2, e2 ) =>
            {
                BrowserHost.AsyncRemoteRun( () => { HostWindow = BrowserHost.Proxy.HWND; } );
            } );
            HeartbeatTimer.Interval = 2000; // 2秒ごと　
            HeartbeatTimer.Start();


            BrowserHost.AsyncRemoteRun( () => BrowserHost.Proxy.GetIconResource() );
        }

        void Exit()
        {
            if ( !BrowserHost.Closed )
            {
                CefSharp.Cef.Shutdown();
                BrowserHost.Close();
                HeartbeatTimer.Stop();
                Application.Exit();
            }
        }

        void BrowserHostChannel_Faulted( Exception e )
        {
            // 親と通信できなくなったら終了する
            Exit();
        }

        public void CloseBrowser()
        {
            HeartbeatTimer.Stop();
            // リモートコールでClose()呼ぶのばヤバそうなので非同期にしておく
            BeginInvoke( (Action)( () => Exit() ) );
        }

        public void ConfigurationChanged( BrowserLib.BrowserConfiguration conf )
        {
            Configuration = conf;

            SizeAdjuster.AutoScroll = Configuration.IsScrollable;
            ToolMenu_Other_Zoom_Fit.Checked = Configuration.ZoomFit;
            ApplyZoom();
            ToolMenu_Other_AppliesStyleSheet.Checked = Configuration.AppliesStyleSheet;
            ToolMenu.Dock = (DockStyle)Configuration.ToolMenuDockStyle;
            ToolMenu.Visible = Configuration.IsToolMenuVisible;

            ToolMenu_Url.Visible = conf.ShowURL && ( ToolMenu.Dock == DockStyle.Top || ToolMenu.Dock == DockStyle.Bottom );

            ToolStripCustomizer.ToolStripRender.RendererTheme = (ToolStripCustomizer.ToolStripRenderTheme)Configuration.ThemeID;

            ToolStripCustomizer.ToolStripRender.SetRender( this.ContextMenuTool );
            ToolStripCustomizer.ToolStripRender.SetRender( this.ToolMenu );
        }

        private void ConfigurationUpdated()
        {
            ToolMenu_Url.Visible = Configuration.ShowURL && ( ToolMenu.Dock == DockStyle.Top || ToolMenu.Dock == DockStyle.Bottom );

            BrowserHost.AsyncRemoteRun( () => BrowserHost.Proxy.ConfigurationUpdated( Configuration ) );
        }

        private void AddLog( int priority, string message )
        {
            BrowserHost.AsyncRemoteRun( () => BrowserHost.Proxy.AddLog( priority, message ) );
        }


        public void InitialAPIReceived()
        {

            IsKanColleLoaded = true;

            //ロード直後の適用ではレイアウトがなぜか崩れるのでこのタイミングでも適用
            ApplyStyleSheet();

            //起動直後はまだ音声が鳴っていないのでミュートできないため、この時点で有効化
            SetVolumeState();
        }


        private void SizeAdjuster_SizeChanged( object sender, EventArgs e )
        {

            if ( !StyleSheetApplied )
            {
                Browser.Location = new Point( 0, 0 );
                Browser.Size = SizeAdjuster.Size;
                return;
            }

            /*/
			Utility.Logger.Add( 1, string.Format( "SizeChanged: BR ({0},{1}) {2}x{3}, PA {4}x{5}, CL {6}x{7}",
				Browser.Location.X, Browser.Location.Y, Browser.Width, Browser.Height, SizeAdjuster.Width, SizeAdjuster.Height, ClientSize.Width, ClientSize.Height ) );
			//*/

            CenteringBrowser();
        }

        private void CenteringBrowser()
        {
            int x = Browser.Location.X, y = Browser.Location.Y;
            bool isScrollable = Configuration.IsScrollable;

            if ( !isScrollable || Browser.Width <= SizeAdjuster.Width )
            {
                x = ( SizeAdjuster.Width - Browser.Width ) / 2;
            }
            if ( !isScrollable || Browser.Height <= SizeAdjuster.Height )
            {
                y = ( SizeAdjuster.Height - Browser.Height ) / 2;
            }

            //if ( x != Browser.Location.X || y != Browser.Location.Y )
            Browser.Location = new Point( x, y );
        }


        /// <summary>
        /// スタイルシートを適用します。
        /// </summary>
        public void ApplyStyleSheet()
        {

            try
            {
                if ( Browser.Address.StartsWith( GAME_URL ) )
                {
                    if ( Configuration.AppliesStyleSheet )
                    {
                        if ( !StyleSheetApplied )
                        {
                            Browser.GetBrowser().MainFrame.ExecuteJavaScriptAsync( FrameScript );

                            StyleSheetApplied = true;
                        }
                    }
                    else if ( StyleSheetApplied )
                    {
                        // remove style
                        Browser.GetBrowser().MainFrame.ExecuteJavaScriptAsync( FrameRestoreScript );

                        StyleSheetApplied = false;
                    }
                }

                ApplyZoom();

            }
            catch ( Exception ex )
            {

                BrowserHost.AsyncRemoteRun( () =>
                    BrowserHost.Proxy.SendErrorReport( ex.ToString(), "样式适用失败。" ) );
            }

        }

        /// <summary>
        /// 指定した URL のページを開きます。
        /// </summary>
        public void Navigate( string url )
        {
            StyleSheetApplied = false;
            //ToolMenu_Url.Text = url;

            Browser.Load( url );
        }

        /// <summary>
        /// ブラウザを再読み込みします。
        /// </summary>
        public void RefreshBrowser()
        {
            Browser.Load( Browser.Address );
        }

        /// <summary>
        /// ズームを適用します。
        /// </summary>
        public void ApplyZoom()
        {
            int zoomRate = Configuration.ZoomRate;

            bool fit = Configuration.ZoomFit && StyleSheetApplied;

            try
            {
                double zoomFactor;

                if ( fit )
                {
                    double rateX = (double)SizeAdjuster.Width / KanColleSize.Width;
                    double rateY = (double)SizeAdjuster.Height / KanColleSize.Height;
                    zoomFactor = Math.Min( rateX, rateY );
                }
                else
                {
                    if ( zoomRate < 10 )
                        zoomRate = 10;
                    if ( zoomRate > 1000 )
                        zoomRate = 1000;

                    zoomFactor = zoomRate / 100.0;
                }

                if ( StyleSheetApplied )
                {
                    Browser.Size = Browser.MinimumSize = new Size(
                        (int)( KanColleSize.Width * zoomFactor ),
                        (int)( KanColleSize.Height * zoomFactor )
                        );
                }
                CenteringBrowser();

                if ( fit )
                {
                    ToolMenu_Other_Zoom_Current.Text = string.Format( "現在: ぴったり" );
                }
                else
                {
                    ToolMenu_Other_Zoom_Current.Text = string.Format( "現在: {0}%", zoomRate );
                }


                // 1.2 ^ (zoomFactor) = zoomLevel
                // zoomFactor = Log(1.2)(zoomLevel)
                var zoomLevel = Math.Log( zoomFactor, 1.2 );

                if ( Browser.IsBrowserInitialized )
                    Browser.GetBrowser().GetHost().SetZoomLevel( zoomLevel );

                //CefSharp.Cef.UIThreadTaskFactory.StartNew( delegate
                //{
                //    var rc = Browser.GetBrowser().GetHost().RequestContext;
                //    var p = (IDictionary<string, object>)rc.GetPreference( "partition" );
                //    p["default_zoom_level"] = new Dictionary<string, object> { { OFFSET_BASIS, 0.5 } };
                //    string error;
                //    bool success = rc.SetPreference( "partition", p, out error );
                //    if ( !success )
                //    {
                //        BrowserHost.AsyncRemoteRun( () =>
                //            BrowserHost.Proxy.SendErrorReport( error, "缩放失败。" ) );
                //    }
                //} );

            }
            catch ( Exception ex )
            {
                //AddLog( 3, "缩放适用失败。" + ex.Message );
                BrowserHost.Proxy.SendErrorReport( ex.StackTrace, "缩放适用失败。" + ex.Message );
            }

        }



        /// <summary>
        /// スクリーンショットを保存します。
        /// </summary>
        /// <param name="folderPath">保存するフォルダへのパス。</param>
        /// <param name="screenShotFormat">スクリーンショットのフォーマット。1=jpg, 2=png</param>
        public void SaveScreenShot( string folderPath, int screenShotFormat )
        {
            if ( !System.IO.Directory.Exists( folderPath ) )
            {
                System.IO.Directory.CreateDirectory( folderPath );
            }

            string ext;
            System.Drawing.Imaging.ImageFormat format;

            switch ( screenShotFormat )
            {
                case 1:
                    ext = "jpg";
                    format = System.Drawing.Imaging.ImageFormat.Jpeg;
                    break;
                case 2:
                default:
                    ext = "png";
                    format = System.Drawing.Imaging.ImageFormat.Png;
                    break;
            }


            SaveScreenShot( string.Format(
                "{0}\\{1:yyyyMMdd_HHmmssff}.{2}",
                folderPath,
                DateTime.Now,
                ext ), format );

        }

        /// <summary>
        /// スクリーンショットを保存します。
        /// </summary>
        /// <param name="path">保存先。</param>
        /// <param name="format">画像のフォーマット。</param>
        private void SaveScreenShot( string path, System.Drawing.Imaging.ImageFormat format )
        {

            //if ( !IsKanColleLoaded ) {
            //	AddLog( 3, string.Format( "艦これが読み込まれていないため、スクリーンショットを撮ることはできません。" ) );
            //	System.Media.SystemSounds.Beep.Play();
            //	return;
            //}

            try
            {
                var client = PointToScreen( Browser.Location );
                if ( ToolMenu.Visible )
                {
                    var width = ToolMenu.Width;
                    var height = ToolMenu.Height;
                    switch ( ToolMenu.Dock )
                    {
                        case DockStyle.Left:
                            client.X += width;
                            break;
                        case DockStyle.Top:
                            client.Y += height;
                            break;
                    }
                }

                using ( var image = new Bitmap( Browser.Width, Browser.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb ) )
                {

                    using ( var g = Graphics.FromImage( image ) )
                    {
                        g.CopyFromScreen( client.X, client.Y, 0, 0, image.Size );
                    }

                    image.Save( path, format );
                }



                AddLog( 2, string.Format( "屏幕截图 {0} 保存完成。", path ) );

            }
            catch ( Exception ex )
            {

                BrowserHost.AsyncRemoteRun( () =>
                    BrowserHost.Proxy.SendErrorReport( ex.ToString(), "屏幕截图时发生错误。" ) );
                System.Media.SystemSounds.Beep.Play();

            }


        }


        public void SetProxy( string proxy )
        {
            CefSharp.Cef.UIThreadTaskFactory.StartNew( delegate
            {
                var rc = Browser.GetBrowser().GetHost().RequestContext;
                string p;
                ushort port;
                if ( ushort.TryParse( proxy, out port ) )
                {
                    p = ( "localhost:" + port );
                }
                else
                {
                    p = ( proxy );
                }
                var v = new Dictionary<string, object>
                {
                    { "mode", "fixed_servers" },
                    { "server", p }
                };
                string error;
                bool success = rc.SetPreference( "proxy", v, out error );
                if ( !success )
                {
                    BrowserHost.AsyncRemoteRun( () =>
                        BrowserHost.Proxy.SendErrorReport( error, "设置代理时发生错误。" ) );
                }
            } );

            //AddLog( 1, "setproxy:" + proxy );
        }

        private void ClearTemporaryInternetFiles()
        {
            MessageBox.Show( "TODO: 请于程序关闭后手动删除cache目录。", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information );
        }


        public void SetIconResource( byte[] canvas )
        {

            SetVolumeState();
        }


        private void SetVolumeState()
        {

            bool mute;
            float volume;

            try
            {
#if !NOVOL
                mute = _volumeManager.IsMute;
                volume = _volumeManager.Volume * 100;
#else
				mute = false;
				isEnabled = false;
#endif

            }
            catch ( Exception )
            {
                // 音量データ取得不能時
                mute = false;
                volume = 100;
            }

            ToolMenu_Mute.Image = ToolMenu_Other_Mute.Image = mute ?
                global::Browser.Properties.Resources.volume_mute :
                global::Browser.Properties.Resources.volume;

            {
                var control = ToolMenu_Other_Volume_VolumeControl;
                control.Tag = false;
                control.Value = (decimal)volume;
                control.Tag = true;
            }

            Configuration.Volume = volume;
            Configuration.IsMute = mute;
            ConfigurationUpdated();
        }

        internal void TakeScreenShot()
        {
            SaveScreenShot( Configuration.ScreenShotPath, Configuration.ScreenShotFormat );
        }

        private void ToolMenu_Other_ScreenShot_Click( object sender, EventArgs e )
        {
            TakeScreenShot();
        }

        private void ToolMenu_Other_Zoom_Decrement_Click( object sender, EventArgs e )
        {
            Configuration.ZoomRate = Math.Max( Configuration.ZoomRate - 20, 10 );
            Configuration.ZoomFit = ToolMenu_Other_Zoom_Fit.Checked = false;
            ApplyZoom();
            ConfigurationUpdated();
        }

        private void ToolMenu_Other_Zoom_Increment_Click( object sender, EventArgs e )
        {
            Configuration.ZoomRate = Math.Min( Configuration.ZoomRate + 20, 1000 );
            Configuration.ZoomFit = ToolMenu_Other_Zoom_Fit.Checked = false;
            ApplyZoom();
            ConfigurationUpdated();
        }

        private void ToolMenu_Other_Zoom_Click( object sender, EventArgs e )
        {

            int zoom;

            if ( sender == ToolMenu_Other_Zoom_25 )
                zoom = 25;
            else if ( sender == ToolMenu_Other_Zoom_50 )
                zoom = 50;
            else if ( sender == ToolMenu_Other_Zoom_75 )
                zoom = 75;
            else if ( sender == ToolMenu_Other_Zoom_100 )
                zoom = 100;
            else if ( sender == ToolMenu_Other_Zoom_150 )
                zoom = 150;
            else if ( sender == ToolMenu_Other_Zoom_200 )
                zoom = 200;
            else if ( sender == ToolMenu_Other_Zoom_250 )
                zoom = 250;
            else if ( sender == ToolMenu_Other_Zoom_300 )
                zoom = 300;
            else if ( sender == ToolMenu_Other_Zoom_400 )
                zoom = 400;
            else
                zoom = 100;

            Configuration.ZoomRate = zoom;
            Configuration.ZoomFit = ToolMenu_Other_Zoom_Fit.Checked = false;
            ApplyZoom();
            ConfigurationUpdated();
        }

        private void ToolMenu_Other_Zoom_Fit_Click( object sender, EventArgs e )
        {
            Configuration.ZoomFit = ToolMenu_Other_Zoom_Fit.Checked;
            ApplyZoom();
            ConfigurationUpdated();
        }


        //ズームUIの使いまわし
        private void ToolMenu_Other_DropDownOpening( object sender, EventArgs e )
        {
            var list = ToolMenu_Zoom.DropDownItems.Cast<ToolStripItem>().ToArray();
            ToolMenu_Other_Zoom.DropDownItems.AddRange( list );
        }

        private void ToolMenu_Zoom_DropDownOpening( object sender, EventArgs e )
        {

            var list = ToolMenu_Other_Zoom.DropDownItems.Cast<ToolStripItem>().ToArray();
            ToolMenu_Zoom.DropDownItems.AddRange( list );
        }


        private void ToolMenu_Other_Mute_Click( object sender, EventArgs e )
        {
#if !NOVOL
            try
            {
                _volumeManager.ToggleMute();

            }
            catch ( Exception )
            {
                System.Media.SystemSounds.Beep.Play();
            }
#endif

            SetVolumeState();
        }

        void ToolMenu_Other_Volume_ValueChanged( object sender, EventArgs e )
        {

            var control = ToolMenu_Other_Volume_VolumeControl;

            try
            {
                if ( (bool)control.Tag )
                    _volumeManager.Volume = (float)( control.Value / 100 );
                control.BackColor = SystemColors.Window;

            }
            catch ( Exception )
            {
                control.BackColor = Color.MistyRose;

            }

        }


        private void ToolMenu_Other_Refresh_Click( object sender, EventArgs e )
        {

            if ( !Configuration.ConfirmAtRefresh ||
                MessageBox.Show( "是否确定刷新网页？", "确认",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2 )
                == System.Windows.Forms.DialogResult.OK )
            {

                RefreshBrowser();
            }
        }

        private void ToolMenu_Other_NavigateToLogInPage_Click( object sender, EventArgs e )
        {

            if ( MessageBox.Show( "是否确认移动到初始页面？", "确认",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2 )
                == System.Windows.Forms.DialogResult.OK )
            {

                Navigate( Configuration.LogInPageURL );
            }

        }

        private void ToolMenu_Other_Navigate_Click( object sender, EventArgs e )
        {
            BrowserHost.AsyncRemoteRun( () => BrowserHost.Proxy.RequestNavigation( string.IsNullOrEmpty( Browser.Address ) ? null : Browser.Address ) );
        }

        private void ToolMenu_Other_AppliesStyleSheet_Click( object sender, EventArgs e )
        {
            Configuration.AppliesStyleSheet = ToolMenu_Other_AppliesStyleSheet.Checked;
            {
                ApplyStyleSheet();
            }
            ConfigurationUpdated();
        }

        private void ToolMenu_Other_Alignment_Click( object sender, EventArgs e )
        {

            if ( sender == ToolMenu_Other_Alignment_Top )
                ToolMenu.Dock = DockStyle.Top;
            else if ( sender == ToolMenu_Other_Alignment_Bottom )
                ToolMenu.Dock = DockStyle.Bottom;
            else if ( sender == ToolMenu_Other_Alignment_Left )
                ToolMenu.Dock = DockStyle.Left;
            else
                ToolMenu.Dock = DockStyle.Right;

            Configuration.ToolMenuDockStyle = (int)ToolMenu.Dock;

            ConfigurationUpdated();
        }

        private void ToolMenu_Other_Alignment_Invisible_Click( object sender, EventArgs e )
        {
            ToolMenu.Visible =
            Configuration.IsToolMenuVisible = false;
            ConfigurationUpdated();
        }


        private void ToolMenu_Other_ClearCache_Click( object sender, EventArgs e )
        {

            if ( MessageBox.Show( "这将会删除浏览器缓存的临时文件,可能需要花费一些时间\n确认要这么做？", "删除缓存", MessageBoxButtons.OKCancel, MessageBoxIcon.Question )
                == System.Windows.Forms.DialogResult.OK )
            {

                //try {

                //    await Task.Factory.StartNew( (Action)( () => ClearCache() ) );
                //    MessageBox.Show( "キャッシュの削除が完了しました。", "削除完了", MessageBoxButtons.OK, MessageBoxIcon.Information );

                //} catch ( Exception ex ) {
                //    BrowserHost.Proxy.SendErrorReport( ex.Message, "清除缓存时出错。" );
                //}

                ClearTemporaryInternetFiles();
                MessageBox.Show( "浏览器缓存已经清空。", "删除缓存成功", MessageBoxButtons.OK, MessageBoxIcon.Information );
            }
        }




        private void SizeAdjuster_DoubleClick( object sender, EventArgs e )
        {
            ToolMenu.Visible =
            Configuration.IsToolMenuVisible = true;
            ConfigurationUpdated();
        }

        private void ContextMenuTool_ShowToolMenu_Click( object sender, EventArgs e )
        {
            ToolMenu.Visible =
            Configuration.IsToolMenuVisible = true;
            ConfigurationUpdated();
        }

        private void ContextMenuTool_Opening( object sender, CancelEventArgs e )
        {

            if ( IsKanColleLoaded || ToolMenu.Visible )
                e.Cancel = true;
        }


        private void ToolMenu_DevTools_Click( object sender, EventArgs e )
        {
            this.Browser.GetBrowser().GetHost().ShowDevTools();
        }

        private void ToolMenu_ScreenShot_Click( object sender, EventArgs e )
        {
            ToolMenu_Other_ScreenShot_Click( sender, e );
        }

        private void ToolMenu_Mute_Click( object sender, EventArgs e )
        {
            ToolMenu_Other_Mute_Click( sender, e );
        }

        private void ToolMenu_Refresh_Click( object sender, EventArgs e )
        {
            ToolMenu_Other_Refresh_Click( sender, e );
        }

        private void ToolMenu_NavigateToLogInPage_Click( object sender, EventArgs e )
        {
            ToolMenu_Other_NavigateToLogInPage_Click( sender, e );
        }



        private void FormBrowser_Activated( object sender, EventArgs e )
        {

            //System.Media.SystemSounds.Asterisk.Play();
            Browser.Focus();
        }

        private void ToolMenu_Other_Alignment_DropDownOpening( object sender, EventArgs e )
        {

            foreach ( var item in ToolMenu_Other_Alignment.DropDownItems )
            {
                var menu = item as ToolStripMenuItem;
                if ( menu != null )
                {
                    menu.Checked = false;
                }
            }

            switch ( (DockStyle)Configuration.ToolMenuDockStyle )
            {
                case DockStyle.Top:
                    ToolMenu_Other_Alignment_Top.Checked = true;
                    break;
                case DockStyle.Bottom:
                    ToolMenu_Other_Alignment_Bottom.Checked = true;
                    break;
                case DockStyle.Left:
                    ToolMenu_Other_Alignment_Left.Checked = true;
                    break;
                case DockStyle.Right:
                    ToolMenu_Other_Alignment_Right.Checked = true;
                    break;
            }

            ToolMenu_Other_Alignment_Invisible.Checked = !Configuration.IsToolMenuVisible;
        }

        protected override void WndProc( ref Message m )
        {

            if ( m.Msg == WM_ERASEBKGND )
                // ignore this message
                return;

            base.WndProc( ref m );
        }


        #region 呪文

        [DllImport( "user32.dll", EntryPoint = "GetWindowLongA", SetLastError = true )]
        private static extern uint GetWindowLong( IntPtr hwnd, int nIndex );

        [DllImport( "user32.dll", EntryPoint = "SetWindowLongA", SetLastError = true )]
        private static extern uint SetWindowLong( IntPtr hwnd, int nIndex, uint dwNewLong );

        private const int GWL_STYLE = ( -16 );
        private const uint WS_CHILD = 0x40000000;
        private const uint WS_VISIBLE = 0x10000000;
        private const int WM_ERASEBKGND = 0x14;

        #endregion

        private void ToolMenu_Mute_DropDownOpening( object sender, EventArgs e )
        {
#if !NOVOL
            trackVolume.Value = (int)( _volumeManager.Volume * 100 );
            trackVolume.Visible = !trackVolume.Visible;
#endif
        }

        private void trackVolume_ValueChanged( object sender, EventArgs e )
        {
#if !NOVOL
            _volumeManager.Volume = trackVolume.Value / 100f;
#endif
        }

        private void trackVolume_LostFocus( object sender, EventArgs e )
        {
#if !NOVOL
            trackVolume.Visible = false;
#endif
        }


    }


}
